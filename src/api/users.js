import client from './client'

export const UpdateAvatar = async (data, userID) => {
  let responseStatus = 200
  await client.put(`/pics/users/${userID}/avatar`, data).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })
  return responseStatus
}

export const UpdateProfile = async (firstName, lastName, userID) => {
  const data = {
    data: {
      attributes: {
        first_name: firstName,
        last_name: lastName
      }
    }
  }

  let responseStatus = 200
  await client.patch(`/pics/users/${userID}/profile`, data).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })
  return responseStatus
}

export const GetUsers = async (filters, size) => {
  const params = new URLSearchParams([['size', size]])
  if (filters.firstName) {
    params.append('filter[first_name]', filters.firstName)
  }
  if (filters.lastName) {
    params.append('filter[last_name]', filters.lastName)
  }
  if (filters.anyName) {
    params.append('filter[all_names]', filters.anyName)
  }
  if (filters.friends) {
    params.append('filter[friends]', filters.friends)
  }

  let responseStatus = 200
  const response = await client.get(`/pics/users`, { params })
    .catch(function (error) {
      if (!error.response) {
        responseStatus = null
      } else {
        responseStatus = error.response.status
      }
    })

  return {
    status: responseStatus,
    data: response
  }
}

export const GetUser = async (id) => {
  let responseStatus = 200
  const response = await client.get(`/pics/users/${id}`).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })
  return {
    status: responseStatus,
    data: response
  }
}

export const AddFriend = async (userID, friendID) => {
  const data = {
    data: {
      id: friendID,
      type: 'user'
    }
  }

  let responseStatus = 200
  await client.post(`/pics/users/${userID}/friends`, data).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })
  return responseStatus
}

export const DeleteFriend = async (userID, friendID) => {
  const data = {
    data: {
      data: {
        id: friendID,
        type: 'user'
      }
    }
  }

  let responseStatus = 200
  await client.delete(`/pics/users/${userID}/friends`, data).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })
  return responseStatus
}
