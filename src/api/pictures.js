import client from './client'
import { UnzipPictures } from '@/js/zip'

export const UploadPicture = async (data) => {
  let responseStatus = 200
  const response = await client.post(`/pics/pictures`, data).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })
  return {
    status: responseStatus,
    data: response
  }
}

export const GetPicsThumbnails = async (page, owner) => {
  let url = `/pics/pictures?page=${page}&size=12`
  if (owner) {
    url += `&filter[owner]=${owner}`
  }

  let responseStatus = 200
  const response = await client.get(url, { responseType: 'arraybuffer' })
    .catch(function (error) {
      if (!error.response) {
        responseStatus = null
      } else {
        responseStatus = error.response.status
      }
    })

  const total = response.headers.total
  const { pictures, picturesID } = await UnzipPictures(response.data)

  return {
    status: responseStatus,
    data: pictures,
    picID: picturesID,
    total: total
  }
}

export const GetFullPicture = async (pictureID) => {
  let responseStatus = 200
  const response = await client.get(`/pics/pictures/${pictureID}`)
    .catch(function (error) {
      if (!error.response) {
        responseStatus = null
      } else {
        responseStatus = error.response.status
      }
    })

  return {
    status: responseStatus,
    data: response
  }
}

export const UpdatePictureOptions = async (pictureID, picOptions) => {
  const data = {
    data: {
      attributes: {
        privacy: picOptions.privacy,
        comment: picOptions.comment
      },
      relationships: {
        shared_with: {
          data: picOptions.sharedWith && picOptions.sharedWith.map(e => ({ id: e, type: "USER" }))
        }
      }
    }
  }

  let responseStatus = 200
  await client.patch(`/pics/pictures/${pictureID}`, data).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })

  return responseStatus
}

export const LikePicture = async (pictureID) => {
  let responseStatus = 200
  await client.post(`/pics/pictures/${pictureID}/like`).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })

  return responseStatus
}

export const UnlikePicture = async (pictureID) => {
  let responseStatus = 200
  await client.delete(`/pics/pictures/${pictureID}/like`).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })

  return responseStatus
}
