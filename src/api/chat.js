import client from '@/api/client'

export const GetChats = async (picID) => {
  const params = new URLSearchParams([['filter[picture]', picID]])

  let responseStatus = 200
  const response = await client.get(`/pics/chats`, { params }).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })

  return {
    status: responseStatus,
    data: response
  }
}

export const GetChatHistory = async (chatID, before, size) => {
  const params = new URLSearchParams([['size', size], ['before', before]])

  let responseStatus = 200
  const response = await client.get(`/pics/chats/${chatID}`, { params }).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })

  return {
    status: responseStatus,
    data: response
  }
}

export const CreateChat = async (picID) => {
  const data = {
    data: {
      id: picID
    }
  }

  let responseStatus = 200
  const response = await client.post(`/pics/chats`, data).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })

  return {
    status: responseStatus,
    data: response
  }
}
