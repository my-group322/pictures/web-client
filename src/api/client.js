import axios from 'axios'
import store from '@/store'
import Vue from 'vue'

const apiEndpoint = 'http://api.friend-art.xyz'

const client = axios.create({
  baseURL: apiEndpoint,
  withCredentials: true
})

/*
  Your can see this block at every function:
  if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
   }
  if (!error.response) catches cases of bad connection and enables
  callers to distinguish it by checking !status or !response.status
 */
client.interceptors.response.use(undefined, function (error) {
  if (!error.response) {
    Vue.toasted.error('Please check your internet connection.')
    return Promise.reject(error)
  } else if (error.response.status === 401) {
    // App component will catch mutation and force sign-in
    store.commit('updateIsAuthed', false)
  } else {
    // Throwing this error further on the chain so other catchers can process it
    return Promise.reject(error)
  }
})

export default client
