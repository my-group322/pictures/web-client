import client from './client'

export const SignUp = async (fields) => {
  const data = {
    data: {
      attributes: {
        email: fields.email,
        password: fields.password,
        first_name: fields.firstName,
        last_name: fields.lastName
      }
    }
  }

  let responseStatus = 200
  const response = await client.post(`/auth/registration`, data).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })
  return {
    status: responseStatus,
    data: response
  }
}

export const LogOut = async () => {
  let responseStatus = 200
  await client.post(`/auth/logout`).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })
  return responseStatus
}

export const LogIn = async (email, password) => {
  const data = {
    data: {
      attributes: {
        email: email,
        password: password
      }
    }
  }

  let responseStatus = 200
  const response = await client.post(`/auth/login`, data).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })
  return {
    status: responseStatus,
    data: response
  }
}

export const GetAccount = async (id) => {
  let responseStatus = 200
  const response = await client.get(`/auth/accounts/${id}`).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })
  return {
    status: responseStatus,
    data: response
  }
}

export const VerifyAccount = async (token) => {
  let responseStatus = 200
  await client.post(`/auth/verification/${token}`, null).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })
  return responseStatus
}

export const RequestAccountVerification = async (id) => {
  let responseStatus = 200
  await client.post(`/auth/accounts/${id}/verification`, null).catch(function (error) {
    if (!error.response) {
      responseStatus = null
    } else {
      responseStatus = error.response.status
    }
  })
  return responseStatus
}
