import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const initialState = () => ({

  userID: '0',
  userEmail: null,
  userFirstName: '',
  userLastName: '',
  userAvatar: '',
  userFriends: [],

  isAuthed: false,
  isVerified: false,

  lastGalleryOwner: null,
  galleryPage: 1,

  usersSharedWith: [],
  picturePrivacyOptions: ['Public', 'Only friends', 'Only selected users']
})

export default new Vuex.Store({
  plugins: [createPersistedState()],

  state: initialState,

  getters: {
    userID: state => state.userID,
    userEmail: state => state.userEmail,
    userFirstName: state => state.userFirstName,
    userLastName: state => state.userLastName,
    userAvatar: state => state.userAvatar,
    userFriends: state => state.userFriends,

    isAuthed: state => state.isAuthed,
    isVerified: state => state.isVerified,

    lastGalleryOwner: state => state.lastGalleryOwner,
    galleryPage: state => state.galleryPage,

    picturePrivacyOptions: state => state.picturePrivacyOptions,
    usersSharedWith: state => state.usersSharedWith
  },

  mutations: {
    resetState: (state) => {
      const initial = initialState()
      Object.keys(initial).forEach(key => { state[key] = initial[key] })
    },

    updateUserID: (state, payload) => Vue.set(state, 'userID', payload),
    updateUserEmail: (state, payload) => Vue.set(state, 'userEmail', payload),
    updateUserFirstName: (state, payload) => Vue.set(state, 'userFirstName', payload),
    updateUserLastName: (state, payload) => Vue.set(state, 'userLastName', payload),
    updateUserAvatar: (state, payload) => Vue.set(state, 'userAvatar', payload),
    updateUserFriends: (state, payload) => Vue.set(state, 'userFriends', payload),
    addUserFriend: (state, payload) => {
      state.userFriends.push(payload)
    },
    deleteUserFriend: (state, payload) => {
      state.userFriends.splice(state.userFriends.indexOf(payload), 1)
    },

    updateIsAuthed: (state, payload) => Vue.set(state, 'isAuthed', payload),
    updateIsVerified: (state, payload) => Vue.set(state, 'isVerified', payload),

    updateLastGalleryOwner: (state, payload) => Vue.set(state, 'lastGalleryOwner', payload),
    updateGalleryPage: (state, payload) => Vue.set(state, 'galleryPage', payload),
    updateUsersSharedWith: (state, payload) => Vue.set(state, 'usersSharedWith', payload)
  }
})
