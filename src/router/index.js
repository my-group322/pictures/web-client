import Vue from 'vue'
import VueRouter from 'vue-router'
import multiguard from 'vue-router-multiguard'
import store from '../store'

import MyGallery from '../pages/MyGallery'
import Login from '../pages/Login'
import Signup from '../pages/Signup'
import VerificationBanner from '../pages/VerificationBanner'
import Verification from '../pages/Verification'
import UploadPicture from '@/pages/UploadPicture'
import FullPicture from '@/pages/FullPicture'
import MyProfile from '@/pages/MyProfile'
import Profile from '@/pages/Profile'
import Pictures from '@/pages/Pictures'
import People from '@/pages/People'

import { GetAccount } from '@/api/auth'

Vue.use(VueRouter)

const isAuthedGuard = (to, from, next) => {
  if (to.name === 'Login' || to.name === 'Signup') {
    next()
  } else if (!store.getters.isAuthed) {
    next({ name: 'Login' })
  } else {
    next()
  }
}

// This guard is assuming that isAuthed check is passed
const isVerifiedGuard = async (to, from, next) => {
  if (!store.getters.isVerified) {
    // Check if account got verified
    const response = await GetAccount(store.getters.userID)
    if (!response.status) {
      return
    }

    if (!response.data.data.data.attributes.is_verified) {
      next({ name: 'VerificationBanner' })
    } else {
      store.commit('updateIsVerified', true)
    }
  } else {
    next()
  }
}

// This thing is for components with nested routes
const Wrapper = {
  template: `
    <div>
      <router-view></router-view>
    </div>
  `
}

const routes = [
  {
    path: '/auth',
    component: Wrapper,
    name: 'Auth',
    children: [
      {
        path: '/sign-in',
        name: 'Login',
        component: Login
      },
      {
        path: '/sign-up',
        name: 'Signup',
        component: Signup
      },
      {
        path: '/verification-banner',
        name: 'VerificationBanner',
        component: VerificationBanner
      },
      {
        path: '/verification/:token',
        component: Verification
      }
    ]
  },
  {
    path: '/',
    beforeEnter: multiguard([
      isAuthedGuard,
      isVerifiedGuard
    ]),
    component: Wrapper,
    name: 'App',
    children: [
      {
        path: 'profile',
        name: 'MyProfile',
        component: MyProfile
      },
      {
        path: 'gallery',
        name: 'Gallery',
        component: MyGallery
      },
      {
        path: 'pictures/',
        name: 'Pictures',
        component: Wrapper,
        children: [
          {
            path: 'all',
            name: 'AllPictures',
            component: Pictures
          },
          {
            path: 'new',
            name: 'UploadPicture',
            component: UploadPicture
          },
          {
            path: 'full',
            name: 'FullPicture',
            component: FullPicture
          }
        ]
      },
      {
        path: 'people/',
        name: 'PeopleWrapper',
        component: Wrapper,
        children: [
          {
            path: 'search',
            name: 'People',
            component: People
          },
          {
            path: ':userID',
            name: 'Profile',
            component: Profile
          }
        ]
      }
    ]
  },
  {
    path: '*',
    redirect: '/gallery',
    component: MyGallery
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
