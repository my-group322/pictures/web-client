import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import Toasted from 'vue-toasted'
import AsyncComputed from 'vue-async-computed'

Vue.config.productionTip = true

Vue.use(AsyncComputed)
Vue.use(Toasted, {
  duration: 4000,
  keepOnHover: true,
  theme: "outline"
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
