import jsZip from 'jszip'

export const UnzipPictures = async (file) => {
  const pictures = []
  const picturesID = []

  const zip = await jsZip.loadAsync(file)
  await Promise.all(Object.keys(zip.files).map(async (filename) => {
    const fileData = await zip.files[filename].async('blob')
    const url = URL.createObjectURL(fileData)
    pictures.push(url)

    const id = filename.substring(0, filename.lastIndexOf('.'))
    picturesID.push(id)
  }))

  return {
    pictures: pictures,
    picturesID: picturesID
  }
}
